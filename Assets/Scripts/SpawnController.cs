﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{

    public GameObject RedCube;
    public GameObject BlueCube;

    public GameObject RedSpawnArea;
    public GameObject BlueSpawnArea;

    float RemainingRed = 5;
    float RemainingBlue = 5;


    bool RedInit = false;
    bool BlueInit = false;


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(PlayerRespawnCoroutine());
        Initialize(0);
        Initialize(1);

        
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.realtimeSinceStartup>5)
        {
            RedInit = true;
            BlueInit = true;
        }

        if (RemainingRed == 0 & RemainingBlue == 0)
        {
            StopCoroutine(PlayerRespawnCoroutine());
        }
    }

    IEnumerator PlayerRespawnCoroutine()
    {
        while (true)
        {
            if (RedInit)
            {
                
                Initialize(0);
                yield return new WaitForSeconds(5f);
                RedInit = false;
            }
            if (BlueInit)
            {
                
                Initialize(1);
                yield return new WaitForSeconds(5f);
                BlueInit = false;
            }
            yield return new WaitForSeconds(1.5f);
        }
    }

    public void SetSpawnFlags(int flagno , bool a)
    {
        if (flagno == 0)
        {
            RedInit = a;
        }
        else
        {
            BlueInit = a;
        }
    }

    public void Initialize(int prefab)
    {

        if (prefab == 0 & RemainingRed != 0)
        {
            Instantiate(RedCube, new Vector3(Random.Range(5.65f, 10.5f), 0.6f, Random.Range(-4.94f, 13f)), Quaternion.identity);
            RemainingRed--;
        }
        else if (prefab == 1 & RemainingBlue != 0)
        {
            Instantiate(BlueCube, new Vector3(Random.Range(-2.22f, -7.75f), 0.6f, Random.Range(-4.94f, 13f)), Quaternion.identity);
            RemainingBlue--;
        }
        else
        {
            Debug.Log("Object Limit Exceeded");
        }
    }
}
