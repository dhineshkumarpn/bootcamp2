﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FactionName { RedFaction, BlueFaction, GreyFaction };

public class Faction : MonoBehaviour
{
    public FactionName factionName;
    GameObject SpawnC;

    GameObject AttackTarget;

    
    Collider[] Overlapping;
    Vector3 center;
    Collider thisCollider;
    bool TargetFound = false;
    private void Start()
    {
        Overlapping = new Collider[10];
        thisCollider = transform.GetComponent<Collider>();
        SpawnC = GameObject.FindGameObjectWithTag("SpawnController");
    }
    private void FixedUpdate()
    {
        ClosestOpponent();
        if (TargetFound)
        {
            //this.transform.GetComponent<RandomMovement>().ChangeForceFlag();
            AddForceTowardsTarget(AttackTarget);
            Color newColor = this.transform.GetComponent<Renderer>().material.color;
            newColor.a = 255;
            this.transform.GetComponent<Renderer>().material.color = Color.black;
            TargetFound = false;
        }
    }
    public void ClosestOpponent()
    {
        if (this.GetComponent<Faction>().factionName != FactionName.GreyFaction)
        {
            center = this.transform.TransformPoint(this.transform.GetComponent<BoxCollider>().center);
            var overlapCount = Physics.OverlapBoxNonAlloc(center, new Vector3(2, 0, 2), Overlapping);
            if (Overlapping.Length > 0)
            {
                // Debug.Log("inside if :"+Overlapping.Length);
                for (int i = 0; i < overlapCount; i++)
                {
                    //   Debug.Log("Before Faction name check");
                    if (Overlapping[i].gameObject.tag.CompareTo("CubeCharacter") == 0)
                    {
                        //     Debug.Log("It is a cube Char");
                        if (Overlapping[i].GetComponent<Faction>().factionName.CompareTo(factionName) != 0 && Overlapping[i].GetComponent<Faction>().factionName.CompareTo(FactionName.GreyFaction) != 0)
                        {
                            //       Debug.Log("Found Closest Collider");
                            AttackTarget = Overlapping[i].gameObject;
                            TargetFound = true;
                        }
                        else
                        {
                            //     Debug.Log("Close collider not found");
                        }
                    }
                }



            }
        }
      
    }

    public void AddForceTowardsTarget(GameObject target)
    {
        GetComponent<Rigidbody>().AddForce(0, 0, 0);
        Vector3 DirectionOfForce = target.transform.position;
        
        GetComponent<Rigidbody>().AddForce(DirectionOfForce * 100, ForceMode.Acceleration);
    }

    

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("CubeCharacter"))
        {
            FactionName otherCharacterFaction = collision.gameObject.GetComponent<Faction>().factionName;
            if (factionName != FactionName.GreyFaction && otherCharacterFaction != FactionName.GreyFaction && otherCharacterFaction != factionName)
            {   
                SpawnC.GetComponent<SpawnController>().SetSpawnFlags(0, true);
                SpawnC.GetComponent<SpawnController>().SetSpawnFlags(1, true);
                Destroy(this.gameObject); 
            }
        }
    }
}
