﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMovement : MonoBehaviour
{
    float startedCollidingAtTime;
    bool CanAddForce = true;
    void Start()
    {
        AddRandomAcceleration();
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.G))
        {
            AddRandomAcceleration();
        }
        //if (!CanAddForce)
        //{
        //    GetComponent<Rigidbody>().AddForce(0,0,0);
        //}
    }

    private void AddRandomAcceleration()
    {
        Vector3 randomDirection = new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f)).normalized;
        GetComponent<Rigidbody>().AddForce(randomDirection * 100, ForceMode.Acceleration);
    }

    private void OnCollisionEnter(Collision collision)
    {
        startedCollidingAtTime = Time.realtimeSinceStartup;
    }

    private void OnCollisionStay(Collision collision)
    {
        if (Time.realtimeSinceStartup - startedCollidingAtTime > 0.1f & CanAddForce) AddRandomAcceleration();
    }

    private void OnCollisionExit(Collision collision)
    {
        AddRandomAcceleration();
    }

    public void ChangeForceFlag()
    {
        CanAddForce = false;
    }


}
